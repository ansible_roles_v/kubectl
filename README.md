kubectl
=========
Installs kubectl - command line tool for communicating with a Kubernetes, using API.

Include role
------------
```yaml
- name: install kubectl
  src: https://gitlab.com/ansible_roles_v/kubectl/
  version: main
```

Example Playbook
----------------
```yaml
- hosts: kubectl
  gather_facts: true
  become: true
  roles:
    - kubectl
```